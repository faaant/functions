const getSum = (str1, str2) => {
  if (!strValidate(str1) || !strValidate(str2)) {
    return false;
  }
  if (!str1.length) {
    return str2;
  }
  if (!str2.length) {
    return str1;
  }

  let result = "";
  let cycles = Math.min(str1.length, str2.length);
  let num = 0;
  for (let i = 1; i<=cycles; i++) {
    let digitSum = +str1[str1.length-i] + +str2[str2.length-i];
    if (num) {
      digitSum+= num;
      num = 0;
    }
    if (digitSum >= 10) {
      digitSum -= 10;
      num++;
    }
    result+= digitSum;
  }
  let maxString = str1.length>str2.length ? str1 : str2;
  for (let i = cycles+1; i<=maxString.length; i++) {
    result += maxString[maxString.length - i];
  }
  return result.split("").reverse().join("");
};

const strValidate = (str) => {
  if (typeof(str)!=="string") {
    return false;
  }

  for (let el of str) {
    if (el<'0' || el>'9') {
      return false;
    }
  }
  return true;
}

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let posts = 0, comments = 0;
  for (let el of listOfPosts) {
    if (el.author === authorName) {
      posts++;
    }
    if (el?.comments) {
      el.comments.forEach(element => {
        if (element.author == authorName) {
          comments++;
        }
      });
    }
  }
  return `Post:${posts},comments:${comments}`;
};

const tickets=(people)=> {
  for (let i=0; i<people.length; i++) {
    if (typeof(people[i])==="string") {
      people[i] = +people[i];
    }
  }
  let banknotes = {
    '25': 0,
    '50': 0,
    '100': 0
  }
  let rest = 0;
  for (let el of people) {
    banknotes[el.toString()]++;
    rest = el - 25;
    if (rest === 75) {
      banknotes["25"]--;
      banknotes["50"]--;
    }
    if (rest === 25) {
      banknotes["25"]--;
    }
    if (banknotes["25"]<0 || banknotes["50"]<0) {
      return "NO";
    }
  }
  return 'YES';
};

module.exports = {getSum, getQuantityPostsByAuthor, tickets};
